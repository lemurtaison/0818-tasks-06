#include <iostream>
#include <stdlib.h>
#include "task06-1.h"

#define FLOOR 1000
#define FENCE 2000
#define RADIUS 3
#define WIDTH 1

using namespace std;

int main() {
	Circle poolwithfloor(RADIUS+WIDTH), pool(RADIUS);
	double count;
	count = ((poolwithfloor.getarea() - pool.getarea())*FLOOR);
	cout << "pay to floor: " << count << endl;
	count += poolwithfloor.getfer()*FENCE;
	cout << "pay to fence: " << count << " rubl" << endl;
	return 0;
}