#define PI 3.1415

#include <math.h>

class Circle {
private:
		double Radius, Ference, Area;
public:
	Circle() {
		Radius = 1.0;
		Ference = 2.0*PI;
		Area = PI;
	}
	Circle(double r){
		Ference = 2 * r*PI;
		Area = PI*r*r;
	}
	double getrad() {
		return Radius;
	}
	double getfer() {
		return Ference;
	}
	double getarea() {
		return Area;
	}
	void setrad(double r) {
		Radius = r;
		Ference = 2 * r*PI;
		Area = PI*r*r;
	}
	void setfer(double f) {
		Ference = f;
		Radius = f / 2 / PI;
		Area = PI*Radius*Radius;
	}
	void setarea(double a) {
		Area = a;
		Radius = sqrt(a / PI);
		Ference = Radius * 2 * PI;
	}
};