#define M_PI       3.14159265358979323846
#include <math.h>
class Circle
{
private:
	double Radius, Ference, Area;
public:
	Circle()
	{
		Radius = NULL;
		Ference = NULL;
		Area = NULL;
	}

	void setRadius(double radius)
	{
		if (radius > 0)
		{
			Radius = radius;
			Ference = 2 * radius * M_PI; 
			Area = M_PI * radius * radius;
		}
	}

	void setFerence(double ference)
	{
		if (ference > 0)
		{
			Radius = ference / (2 * M_PI);
			Ference = ference; 
			Area = M_PI * Radius * Radius;
		}
	}

	void setArea(double area)
	{
		if (area > 0)
		{
			Radius = sqrt(area / M_PI);
			Ference = 2 * Radius * M_PI; 
			Area = area;
		}
	}

	double getRadius()
	{
		return Radius;
	}

	double getFerence()
	{
		return Ference;
	}

	double getArea()
	{
		return Area;
	}

};