#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <stdlib.h>
using namespace std;

class Circle {
private:
	enum circleInfo {
		radius,
		ference,
		area
	};
	double Radius;
	double Ference;
	double Area;

	void ReCompute(circleInfo type) {
		switch (type) {
		case radius:
			Ference = 2.0 * M_PI * Radius;
			Area = M_PI * pow(M_PI, 2);
			break;
		case ference:
			Radius = Ference * 0.5 / M_PI;
			Area = M_PI * pow(Radius, 2);
			break;
		case area:
			Radius = sqrt(Area / M_PI);
			Ference = 2.0 * M_PI * Radius;
			break;
		}
	}

public:
	Circle(double rad) {
		SetRadius(rad);
	}
	~Circle();
	void SetRadius(double Radius) {
		Circle::Radius = Radius;
		ReCompute(radius);
	}
	double GetRadius() {
		return Radius;
	}
	void SetFerence(double Ference) {
		Circle::Ference = Ference;
		ReCompute(ference);
	}
	double GetFerence() {
		return Ference;
	}
	void SetArea(double Area) {
		Circle::Area = Area;
		ReCompute(area);
	}
	double GetArea() {
		return Area;
	}
};