#define _USE_MATH_DEFINES
#include <iostream>
#include <iomanip>
#include "task06-1.h"

using namespace std;

int main()
{
	int road_price = 1000, fence_price = 2000, pool_radius = 3, width_of_road = 1;
	double cost_of_road, cost_of_fence;

	Circle pool(pool_radius), pool_with_road(pool_radius + width_of_road);

	cost_of_road = road_price * (pool_with_road.getArea() - pool.getArea());
	cost_of_fence = fence_price * pool_with_road.getFerence();

	cout << "Cost of materials: " << fixed << setprecision(2) << cost_of_road + cost_of_fence << " rub.";

	return 0;
}